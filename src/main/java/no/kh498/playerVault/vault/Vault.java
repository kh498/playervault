package no.kh498.playerVault.vault;

import com.google.common.base.Preconditions;
import no.kh498.playerVault.Locale;
import no.kh498.playerVault.PlayerVault;
import no.kh498.util.itemMenus.api.ListItemMenu;
import no.kh498.util.itemMenus.api.MenuItem;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class Vault {

    private static Logger logger = LoggerFactory.getLogger(Vault.class);

    private Map<UUID, List<ItemStack>> sections;

    public Vault() {

        YamlConfiguration vault = getVaultStorage();

        sections = new HashMap<>();
        Set<String> uuids = vault.getConfigurationSection("").getKeys(false);
        for (String uuid : uuids) {
            //noinspection unchecked
            List<ItemStack> items = ((List<ItemStack>) vault.get(uuid));
            logger.trace("uuid: {} items: {}", uuid, items);
            sections.put(UUID.fromString(uuid), items);
        }

    }

    public void open(Player player) {
        Preconditions.checkNotNull(player, "Cannot open inventory to null player");

        List<ItemStack> items = sections.computeIfAbsent(player.getUniqueId(), k -> new ArrayList<>());
        ArrayList<MenuItem> menuItems = new ArrayList<>();

        for (ItemStack item : items) {
            if (item.getAmount() <= 0) {
                continue;
            }
            menuItems.add(new ItemStackMenuItem(item));
        }
        new ListItemMenu(menuItems, Locale.TITLE.toString()).setSmoothUpdate(PlayerVault.useSmoothPageUpdate())
                                                            .openMenu(player);
    }

    public void addItems(UUID uuid, List<ItemStack> items) {
        sections.computeIfAbsent(uuid, k -> new ArrayList<>()).addAll(items);
    }

    private File getVaultFile() {
        File vault = new File(PlayerVault.inst().getDataFolder(), "vault.yml");
        if (!vault.exists()) {
            try {
                //noinspection ResultOfMethodCallIgnored
                vault.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return vault;
    }

    private YamlConfiguration getVaultStorage() {
        File vault = getVaultFile();
        return YamlConfiguration.loadConfiguration(vault);
    }

    public void save() {
        //clean items
        for (List<ItemStack> items : sections.values()) {
            items.removeIf(is -> is == null || is.getAmount() <= 0 || is.getType() == Material.AIR);
        }

        //save to disk
        YamlConfiguration vault = getVaultStorage();

        for (Map.Entry<UUID, List<ItemStack>> entry : sections.entrySet()) {
            vault.set(entry.getKey().toString(), entry.getValue());
        }

        try {
            vault.save(getVaultFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
