package no.kh498.playerVault.vault;

import no.kh498.util.itemMenus.api.MenuItem;
import no.kh498.util.itemMenus.events.ItemClickEvent;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class ItemStackMenuItem extends MenuItem {

    private static Logger logger = LoggerFactory.getLogger(Vault.class);

    private ItemStack realItem;
    private boolean claimed;

    public ItemStackMenuItem(ItemStack itemStack) {
        super(itemStack.getItemMeta().getDisplayName(), itemStack, itemStack.getItemMeta().getLore());
        realItem = itemStack;
        claimed = false;
    }

    @Override
    public void onItemClick(ItemClickEvent event) {
        if (claimed) {
            logger.trace("Already claimed");
            return;
        }
        HashMap<Integer, ItemStack> leftOverMap = event.getPlayer().getInventory().addItem(realItem);
        if (leftOverMap.isEmpty()) {
            logger.trace("everything is taken");
            setIcon(new ItemStack(Material.AIR));
            realItem.setAmount(0);
            claimed = true;

        }
        else {
            ItemStack leftOvers = leftOverMap.get(0);
            logger.trace("Leftover item {}", leftOvers);
            realItem = leftOvers.clone();
            setIcon(leftOvers);
        }
        event.setWillUpdate(true);
    }
}
