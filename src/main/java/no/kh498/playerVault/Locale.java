package no.kh498.playerVault;

import no.kh498.util.ChtUtil;

public enum Locale {

    TITLE("locale.title", "Reward"),
    FULL_INVENTORY("locale.fullInv", "&7There is no room for your whole reward! To retrieve it do &b/havenvault");


    private final String path;
    private final String defaultStr;
    private String content;

    Locale(String path, String defaultStr) {
        this.path = path;
        this.defaultStr = defaultStr;
    }

    public static void reload() {
        for (Locale loc : values()) {
            loc.content = ChtUtil.toBukkitColor(PlayerVault.inst().getConfig().getString(loc.path, loc.defaultStr));
        }
    }

    @Override
    public String toString() {
        return content;
    }
}
