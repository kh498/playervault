package no.kh498.playerVault;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

public class Listener implements org.bukkit.event.Listener {

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onCommand(PlayerCommandPreprocessEvent event) {
        boolean cancel = PlayerVault.onCommand(event.getPlayer(), event.getMessage());
        event.setCancelled(cancel);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onConsoleCmd(ServerCommandEvent event) {
        boolean cancel = PlayerVault.onCommand(event.getSender(), event.getCommand());
        event.setCancelled(cancel);
    }
}
