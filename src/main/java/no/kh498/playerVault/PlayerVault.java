package no.kh498.playerVault;

import no.kh498.playerVault.util.Util;
import no.kh498.playerVault.vault.Vault;
import no.kh498.util.ChtUtil;
import no.kh498.util.itemMenus.PluginHolder;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.BukkitLoggerAdapter;

import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

public class PlayerVault extends JavaPlugin {

    private final static String CMD_LOOK_PATH = "giveCommands";

    private static Logger logger = LoggerFactory.getLogger(PlayerVault.class);

    private static FakePlayerPool pool;

    private static PlayerVault inst;

    //must be lowercase
    private static Collection<Pattern> whitelistedCommands;

    private Vault vault;

    private static boolean smoothPageUpdate = true;

    @Override
    public void onEnable() {
        inst = this;

        saveDefaultConfig();
        reloadConfig();
        BukkitLoggerAdapter.init(true);

        PluginHolder.setPlugin(this);

        pool = new FakePlayerPool(1);

        Bukkit.getPluginManager().registerEvents(new Listener(), this);
        Bukkit.getPluginManager().registerEvents(pool, this);

        smoothPageUpdate = getConfig().getBoolean("UseSmoothPageUpdate", smoothPageUpdate);

        List<String> cmdRules = getConfig().getStringList(CMD_LOOK_PATH);
        whitelistedCommands = new HashSet<>();

        for (String rule : cmdRules) {
            try {
                whitelistedCommands.add(Pattern.compile(rule, Pattern.CASE_INSENSITIVE));
            } catch (PatternSyntaxException e) {
                logger.error("Failed to compile regex pattern {} : {}", rule, e.getDescription());
            }
        }

        logger.trace("whitelistedCommands: " + whitelistedCommands);
        vault = new Vault();

        getCommand("havenvault").setExecutor((sender, command, label, args) -> {
            if (sender instanceof ConsoleCommandSender) {
                sender.sendMessage("Console cannot get rewards :(");
                return true;
            }
            vault.open((Player) sender);
            return true;
        });

        Locale.reload();
    }

    @Override
    public void onDisable() {
        vault.save();
        pool.onShutdown();
    }


    /**
     * @param sender
     *     The sender of the command
     * @param cmd0
     *     The command string without a slash
     *
     * @return If the command given should be cancelled or not
     */
    public static boolean onCommand(CommandSender sender, String cmd0) {
        logger.trace("sender = [" + sender + "], msg = [" + cmd0 + "]");

        if (cmd0.startsWith("/")) { cmd0 = cmd0.substring(1); }
        String cmd = cmd0.trim();

        String[] cmdParts = cmd.split(" ");
        if ((cmdParts[0].equalsIgnoreCase("kick") || cmdParts[0].equalsIgnoreCase("ban")) &&
            cmdParts[1].startsWith(FakePlayerPool.FAKE_PLAYER_NAME.toLowerCase())) {
            sender.sendMessage(ChatColor.RED + "You cannot kick a fake player!");
            return true;
        }

        if (whitelistedCommands.stream().noneMatch(pattern -> pattern.matcher(cmd).matches())) {
            logger.debug("Command not whitelisted '{}'", cmd);
            return false;
        }

        Player fakePlayer = pool.get();
        if (fakePlayer == null) {
            logger.error("Fake player is null!");
            return false;
        }
        logger.debug("Using fake player {}", fakePlayer.getName());

        //first part cannot be a player name
        StringBuilder newCmdBuilder = new StringBuilder(cmdParts[0] + " ");

        OfflinePlayer player = null;

        for (int i = 1; i < cmdParts.length; i++) {
            //ignore double spaces as they give no new information
            if (StringUtils.isBlank(cmdParts[i])) {
                continue;
            }

            //Look for the real player
            if (player == null) {
                OfflinePlayer partPlayer = Bukkit.getOfflinePlayer(cmdParts[i]);
                if (Util.hasPlayedBefore(partPlayer)) {
                    logger.trace("Found real player at cmdParts[{}] = {}", i, cmdParts[i]);
                    cmdParts[i] = fakePlayer.getName();
                    player = partPlayer;
                }
            }
            newCmdBuilder.append(cmdParts[i]).append(" ");
        }

        if (player == null) {
            logger.debug("No real player found in command {}", cmd);
            return false;
        }

        String newCmd = newCmdBuilder.toString().trim();
        logger.trace("newcmd = '{}'", newCmd);

        Bukkit.dispatchCommand(sender, newCmd);

        UUID uuid = player.getUniqueId();

        Bukkit.getScheduler().scheduleSyncDelayedTask(PlayerVault.inst, () -> {

            //get all non-null items of fake player, these are the items that the command gave
            List<ItemStack> items = Arrays.stream(fakePlayer.getInventory().getContents()).filter(Objects::nonNull)
                                          .collect(Collectors.toList());

            Player plr = Bukkit.getPlayer(uuid);

            //Decide what items should be added to the vault
            Collection<ItemStack> leftOver;
            if (plr != null && !plr.isDead()) {
                Map<Integer, ItemStack> leftoverMap = plr.getInventory().addItem(items.toArray(new ItemStack[0]));
                leftOver = leftoverMap.values();
            }
            else {
                leftOver = items;
            }

            if (!leftOver.isEmpty()) {
                PlayerVault.inst.vault.addItems(uuid, items);
                if (plr != null) {
                    ChtUtil.sendFormattedMsg(plr, Locale.FULL_INVENTORY.toString());
                }
            }

            //cleanup
            pool.returnPlayer(fakePlayer);
        }, 5L);
        return true;
    }

    public static PlayerVault inst() {
        return inst;
    }

    public static boolean useSmoothPageUpdate() {
        return smoothPageUpdate;
    }
}
