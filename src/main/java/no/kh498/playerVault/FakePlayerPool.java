package no.kh498.playerVault;

import net.minecraft.server.v1_12_R1.DedicatedPlayerList;
import net.minecraft.server.v1_12_R1.EntityPlayer;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerInfo;
import no.kh498.playerVault.util.Util;
import no.kh498.util.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.TabCompleteEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class FakePlayerPool implements Listener {

    private static Logger logger = LoggerFactory.getLogger(FakePlayerPool.class);

    private static final boolean REUSE_FAKE_PLAYERS = true;
    private static final boolean MODIFY_MAX_PLAYERS = false;

    static final String FAKE_PLAYER_NAME = "FAKE-PLAYER";

    private HashMap<Player, Boolean> players;
    private int playerNr;

    public FakePlayerPool(int initialSize) {
        players = new HashMap<>();
        logger.debug("Reuse fake players? {}", REUSE_FAKE_PLAYERS);
        if (REUSE_FAKE_PLAYERS) {
            for (int i = 0; i < initialSize; i++) {
                players.put(create(), true);
            }
        }
    }

    private Player create() {
        String name = FAKE_PLAYER_NAME + (playerNr++);
        logger.debug("Growing fake player pool, new name will be {}", name);
        Player plr = Util.createFakePlayer(name);

        changeMaxPlayers(1);
        return plr;
    }

    public Player get() {
        if (REUSE_FAKE_PLAYERS) {
            for (Map.Entry<Player, Boolean> entry : players.entrySet()) {
                if (entry.getValue()) {
                    entry.setValue(false);
                    return entry.getKey();
                }
            }
        }
        //noinspection UnusedAssignment
        Player fake = create();
        players.put(fake, false);
        return fake;
    }

    public void returnPlayer(Player player) {
        if (!REUSE_FAKE_PLAYERS) {
            player.kickPlayer("");
            player.remove();
            players.remove(player);
        }
        else {
            player.getInventory().clear();
            players.computeIfPresent(player, (p, b) -> true);
        }
    }

    /////////////////////////////////////////////////////////////////////////////////

    @EventHandler
    public void onQuit(PlayerQuitEvent event) {
        logger.trace("player {} quit", event.getPlayer().getName());
        if (event.getPlayer().getName().startsWith(FAKE_PLAYER_NAME)) {
            event.setQuitMessage("");
            Util.deletePlayerProfile(event.getPlayer().getUniqueId());
            changeMaxPlayers(-1);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        if (player.getName().startsWith(FAKE_PLAYER_NAME)) {
            logger.trace("Did not update tab list of fake player");
            return;
        }
        for (Player fake : players.keySet()) {
            player.hidePlayer(PlayerVault.inst(), fake);

            EntityPlayer epFake = ((CraftPlayer) fake).getHandle();
            PacketPlayOutPlayerInfo pack =
                new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, epFake);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(pack);
        }
    }

    @EventHandler
    public void onTabComplete(TabCompleteEvent event) {
        ArrayList<String> newComp = new ArrayList<>();
        for (String completion : event.getCompletions()) {
            if (completion.startsWith(FAKE_PLAYER_NAME)) {
                continue;
            }
            newComp.add(completion);
        }
        event.setCompletions(newComp);
    }

    public void onShutdown() {
        for (Player player : players.keySet()) {
            player.kickPlayer("");
            Util.deletePlayerProfile(player.getUniqueId());
        }
    }

    private void changeMaxPlayers(int delta) {
        if (MODIFY_MAX_PLAYERS) {
            DedicatedPlayerList playerList = ((CraftServer) Bukkit.getServer()).getHandle();
            try {
                Reflection.modifySuperField(playerList, "maxPlayers", playerList.getMaxPlayers() + delta);
            } catch (NoSuchFieldException | IllegalAccessException e) {
                logger.error("Failed to changed max players due to a {}, enable debug logger for more info",
                             e.getClass().getSimpleName());
                if (logger.isDebugEnabled()) {
                    e.printStackTrace();
                }
            }
        }
    }
}
