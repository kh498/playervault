package no.kh498.playerVault.util;

import com.mojang.authlib.GameProfile;
import net.minecraft.server.v1_12_R1.*;
import no.kh498.util.Reflection;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_12_R1.CraftServer;
import org.bukkit.entity.Player;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.UUID;

public class Util {

    public static boolean hasPlayedBefore(OfflinePlayer player) {
        if (player.hasPlayedBefore()) { return true; }
        for (OfflinePlayer realOfflinePlayer : Bukkit.getOfflinePlayers()) {
            if (realOfflinePlayer.getUniqueId().equals(player.getUniqueId())) {
                return true;
            }
        }
        return false;
    }

    public static Player createFakePlayer(String name) {
        CraftServer craftServer = ((CraftServer) Bukkit.getServer());

        MinecraftServer mcs = craftServer.getServer();
        WorldServer ws = mcs.getWorldServer(0);
        GameProfile gp = new GameProfile(UUID.randomUUID(), name);
        PlayerInteractManager pim = new PlayerInteractManager(ws);

        EntityPlayer ep = new EntityPlayer(mcs, ws, gp, pim);

        try {
            Map<String, EntityPlayer> playersByName =
                (Map<String, EntityPlayer>) Reflection.getSuperField(craftServer.getHandle(), "playersByName");
            playersByName.put(ep.getName(), ep);

            Map<UUID, EntityPlayer> playersByUuid =
                (Map<UUID, EntityPlayer>) Reflection.getSuperField(craftServer.getHandle(), "j"); //playersByUUID
            playersByUuid.put(ep.getUniqueID(), ep);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }

        return ep.getBukkitEntity();
    }

    public static void deletePlayerProfile(UUID uuid) {
        CraftServer craftServer = ((CraftServer) Bukkit.getServer());
        WorldNBTStorage playerFileData =
            (WorldNBTStorage) craftServer.getHandle().getServer().worlds.get(0).getDataManager().getPlayerFileData();
        File playerData = new File(playerFileData.getPlayerDir(), uuid.toString() + ".dat");

        try {
            Files.deleteIfExists(Paths.get(playerData.toURI()));
        } catch (IOException e) {
            playerData.deleteOnExit();
            e.printStackTrace();
        }
    }


}
