package no.kh498.playerVault.util;

import net.minecraft.server.v1_12_R1.EnumProtocolDirection;
import net.minecraft.server.v1_12_R1.NetworkManager;

public class SilentNetworkManager extends NetworkManager {

    public SilentNetworkManager(EnumProtocolDirection enumprotocoldirection) {
        super(enumprotocoldirection);
    }

    @Override
    public void stopReading() {
        if (channel == null || channel.config() == null) {
            return;
        }
        this.channel.config().setAutoRead(false);
    }
}
