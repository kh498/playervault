package no.kh498.playerVault.util.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import net.minecraft.server.v1_12_R1.MojangsonParseException;
import net.minecraft.server.v1_12_R1.MojangsonParser;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import no.kh498.playerVault.util.gson.GsonFactory;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.craftbukkit.v1_12_R1.util.CraftMagicNumbers;
import org.bukkit.inventory.ItemStack;

import java.io.IOException;

/**
 * @author kh498
 * @since 0.1.0
 */
public class NewItemStackAdapter extends TypeAdapter<ItemStack> {

    @Override
    public void write(final JsonWriter jsonWriter, final ItemStack itemStack) throws IOException {
        if (itemStack == null) {
            jsonWriter.nullValue();
            return;
        }
        final net.minecraft.server.v1_12_R1.ItemStack item = GsonFactory.removeSlot(itemStack);
        if (item == null) {
            jsonWriter.nullValue();
            return;
        }
        try {
            jsonWriter.beginObject();
            jsonWriter.name("type");

            jsonWriter.value(itemStack.getType().toString()); // I hate using this - but
            jsonWriter.name("amount");

            jsonWriter.value(itemStack.getAmount());
            jsonWriter.name("data");

            jsonWriter.value(itemStack.getDurability());
            jsonWriter.name("tag");

            if (item.getTag() != null) {
                jsonWriter.value(GsonFactory.nbtToString(item.getTag()));
            }
            else {
                jsonWriter.value("");
            }
            jsonWriter.endObject();
        } catch (final Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public ItemStack read(final JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            return null;
        }
        jsonReader.beginObject();
        jsonReader.nextName();
        final Material type = Material.getMaterial(jsonReader.nextString());
        jsonReader.nextName();
        final int amount = jsonReader.nextInt();
        jsonReader.nextName();
        final int data = jsonReader.nextInt();
        final net.minecraft.server.v1_12_R1.ItemStack item =
            new net.minecraft.server.v1_12_R1.ItemStack(CraftMagicNumbers.getItem(type), amount, data);
        jsonReader.nextName();
        final String next = jsonReader.nextString();
        if (next.startsWith("{")) {
            NBTTagCompound compound = null;
            try {
                compound = MojangsonParser.parse(ChatColor.translateAlternateColorCodes('&', next));
            } catch (final MojangsonParseException e) {
                e.printStackTrace();
            }
            item.setTag(compound);
        }
        jsonReader.endObject();
        return CraftItemStack.asBukkitCopy(item);
    }
}
