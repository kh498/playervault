package no.kh498.playerVault.util.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import no.kh498.playerVault.util.gson.GsonFactory;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Joshua Bell (RingOfStorms)
 * @since 0.1.0
 */
public class ItemStackGsonAdapter extends TypeAdapter<ItemStack> {

    private static final Type seriType = new TypeToken<Map<String, Object>>() { }.getType();

    private String getRaw(final ItemStack item) {
        final Map<String, Object> serial = item.serialize();

        if (serial.get("meta") != null) {
            final ItemMeta itemMeta = item.getItemMeta();

            final Map<String, Object> originalMeta = itemMeta.serialize();
            final Map<String, Object> meta = new HashMap<>();
            for (final Map.Entry<String, Object> entry : originalMeta.entrySet()) {
                meta.put(entry.getKey(), entry.getValue());
            }
            Object o;
            for (final Map.Entry<String, Object> entry : meta.entrySet()) {
                o = entry.getValue();
                if (o instanceof ConfigurationSerializable) {
                    final ConfigurationSerializable serializable = (ConfigurationSerializable) o;
                    final Map<String, Object> serialized = GsonFactory.recursiveSerialization(serializable);
                    meta.put(entry.getKey(), serialized);
                }
            }
            serial.put("meta", meta);
        }

        return GsonFactory.defaultGson.toJson(serial);
    }

    private ItemStack fromRaw(final String raw) {
        final Map<String, Object> keys = GsonFactory.defaultGson.fromJson(raw, seriType);

        if (keys.get("amount") != null) {
            final Double d = (Double) keys.get("amount");
            final Integer i = d.intValue();
            keys.put("amount", i);
        }

        final ItemStack item;
        try {
            item = ItemStack.deserialize(keys);
        } catch (final Exception e) {
            return null;
        }

        if (item == null) {
            return null;
        }

        if (keys.containsKey("meta")) {
            Map<String, Object> itemmeta = (Map<String, Object>) keys.get("meta");
            itemmeta = GsonFactory.recursiveDoubleToInteger(itemmeta);
            final ItemMeta meta = (ItemMeta) ConfigurationSerialization
                .deserializeObject(itemmeta, ConfigurationSerialization.getClassByAlias("ItemMeta"));
            item.setItemMeta(meta);
        }

        return item;
    }

    @Override
    public void write(final JsonWriter jsonWriter, final ItemStack itemStack) throws IOException {
        if (itemStack == null) {
            jsonWriter.nullValue();
            return;
        }
        jsonWriter.value(getRaw(GsonFactory.removeSlotNBT(itemStack)));
    }

    @Override
    public ItemStack read(final JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        return fromRaw(jsonReader.nextString());
    }


}
