package no.kh498.playerVault.util.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author kh498
 * @since 0.1.0
 */
public class LocationGsonAdapter extends TypeAdapter<Location> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static final String UUID = "WORLD_UUID";
    private static final String X = "X";
    private static final String Y = "Y";
    private static final String Z = "Z";
    private static final String YAW = "YAW";
    private static final String PITCH = "PITCH";

    @Override
    public void write(final JsonWriter writer, final Location location) throws IOException {
        if (location == null) {
            writer.nullValue();
            return;
        }
        writer.beginObject();

        writer.name(UUID).value(location.getWorld().getUID().toString());
        writer.name(X).value(location.getX());
        writer.name(Y).value(location.getY());
        writer.name(Z).value(location.getZ());
        writer.name(YAW).value(location.getYaw());
        writer.name(PITCH).value(location.getPitch());

        writer.endObject();
    }

    @Override
    public Location read(final JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }

        World world = null;
        double x = 0D;
        double y = 0D;
        double z = 0D;
        float yaw = 0F;
        float pitch = 0F;
        reader.beginObject();
        while (reader.hasNext()) {
            final String name = reader.nextName();
            switch (name) {
                case UUID:
                    world = Bukkit.getWorld(java.util.UUID.fromString(reader.nextString()));
                    break;
                case X:
                    x = reader.nextDouble();
                    break;
                case Y:
                    y = reader.nextDouble();
                    break;
                case Z:
                    z = reader.nextDouble();
                    break;
                case YAW:
                    yaw = (float) reader.nextDouble();
                    break;
                case PITCH:
                    pitch = (float) reader.nextDouble();
                    break;
                default:
                    this.log.warn("Failed to find the correct value for " + name);

            }
        }
        reader.endObject();
        if (world == null) {
            return null;
        }
        return new Location(world, x, y, z, yaw, pitch);
    }


}
