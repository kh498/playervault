package no.kh498.playerVault.util.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import net.minecraft.server.v1_12_R1.NBTBase;
import net.minecraft.server.v1_12_R1.NBTTagCompound;
import no.kh498.playerVault.util.gson.adapters.*;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * Created by Joshua Bell (RingOfStorms)
 * <p>
 * Post explaining here: http://bukkit.org/threads/gsonfactory-gson-that-works-on-itemstack-potioneffect-location
 * -objects.331161/
 *
 * @author Joshua Bell (RingOfStorms)
 * @author kh498
 * @since 0.1.0
 */
public class GsonFactory {

    public static final Gson defaultGson = new Gson();

    private static final boolean PRETTY_PRINTING = true;

    /*
     * - I want to not use Bukkit parsing for most objects... it's kind of clunky - Instead... I want
     * to start using any of Mojang's tags - They're really well documented + built into MC, and
     * handled by them. - Rather than kill your old code, I'm going to write TypeAdapters using
     * Mojang's stuff.
     */
    private final static String CLASS_KEY = "SERIAL-ADAPTER-CLASS-KEY";
    private static Gson gson;

    /**
     * Creates a new instance of Gson for use anywhere <p> Use {@code @GsonIgnore} in order to skip serialization and
     * deserialization </p>
     *
     * @return a Gson instance
     */
    public static Gson getGson() {
        if (gson != null) {
            return gson;
        }

        final GsonBuilder builder = new GsonBuilder().disableHtmlEscaping().serializeNulls();

        //ExclusionStrategy
        builder.addSerializationExclusionStrategy(new ExposeExclusion());
        builder.addDeserializationExclusionStrategy(new ExposeExclusion());

        //TypeHierarchyAdapter
        builder.registerTypeHierarchyAdapter(ItemStack.class, new ItemStackGsonAdapter());

        //TypeAdapters
        builder.registerTypeAdapter(Location.class, new LocationGsonAdapter());
        builder.registerTypeAdapter(Date.class, new DateGsonAdapter());
        builder.registerTypeAdapter(ItemStack.class, new ItemStackGsonAdapter());
        builder.registerTypeAdapter(OfflinePlayer.class, new OfflinePlayerAdapter());
        builder.registerTypeAdapter(Vector.class, new VectorAdapter());

        if (PRETTY_PRINTING) {
            builder.setPrettyPrinting();
        }

        return gson = builder.create();
    }

    public static Map<String, Object> recursiveSerialization(final ConfigurationSerializable o) {
        final Map<String, Object> originalMap = o.serialize();
        final Map<String, Object> map = new HashMap<>();
        for (final Entry<String, Object> entry : originalMap.entrySet()) {
            final Object o2 = entry.getValue();
            if (o2 instanceof ConfigurationSerializable) {
                final ConfigurationSerializable serializable = (ConfigurationSerializable) o2;
                final Map<String, Object> newMap = recursiveSerialization(serializable);
                newMap.put(CLASS_KEY, ConfigurationSerialization.getAlias(serializable.getClass()));
                map.put(entry.getKey(), newMap);
            }
        }
        map.put(CLASS_KEY, ConfigurationSerialization.getAlias(o.getClass()));
        return map;
    }

    public static Map<String, Object> recursiveDoubleToInteger(final Map<String, Object> originalMap) {
        final Map<String, Object> map = new HashMap<>();
        for (final Entry<String, Object> entry : originalMap.entrySet()) {
            final Object value = entry.getValue();
            if (value instanceof Double) {
                final Double d = (Double) value;
                final Integer i = d.intValue();
                map.put(entry.getKey(), i);
            }
            else if (value instanceof Map) {
                @SuppressWarnings("unchecked") final Map<String, Object> subMap = (Map<String, Object>) value;
                map.put(entry.getKey(), recursiveDoubleToInteger(subMap));
            }
            else {
                map.put(entry.getKey(), value);
            }
        }
        return map;
    }

    public static String nbtToString(final NBTBase base) {
        return base.toString().replace(",}", "}").replace(",]", "]").replaceAll("[0-9]+:", "");
    }

    public static net.minecraft.server.v1_12_R1.ItemStack removeSlot(final ItemStack item) {
        if (item == null) {
            return null;
        }
        final net.minecraft.server.v1_12_R1.ItemStack nmsi = CraftItemStack.asNMSCopy(item);
        if (nmsi == null) {
            return null;
        }
        final NBTTagCompound nbtt = nmsi.getTag();
        if (nbtt != null) {
            nbtt.remove("Slot");
            nmsi.setTag(nbtt);
        }
        return nmsi;
    }

    public static ItemStack removeSlotNBT(final ItemStack item) {
        if (item == null) {
            return null;
        }
        final net.minecraft.server.v1_12_R1.ItemStack nmsi = CraftItemStack.asNMSCopy(item);
        if (nmsi == null) {
            return null;
        }
        final NBTTagCompound nbtt = nmsi.getTag();
        if (nbtt != null) {
            nbtt.remove("Slot");
            nmsi.setTag(nbtt);
        }
        return CraftItemStack.asBukkitCopy(nmsi);
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.FIELD})
    public @interface GsonIgnore { }

    public static class ExposeExclusion implements ExclusionStrategy {

        @Override
        public boolean shouldSkipField(final FieldAttributes fieldAttributes) {
            final GsonIgnore ignore = fieldAttributes.getAnnotation(GsonIgnore.class);
            //noinspection VariableNotUsedInsideIf
            if (ignore != null) {
                return true;
            }
            final Expose expose = fieldAttributes.getAnnotation(Expose.class);
            return expose != null && (!expose.serialize() || !expose.deserialize());
        }

        @Override
        public boolean shouldSkipClass(final Class<?> aClass) {
            return false;
        }
    }
}



