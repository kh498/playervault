package no.kh498.playerVault.util.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.io.IOException;
import java.util.UUID;

public class OfflinePlayerAdapter extends TypeAdapter<OfflinePlayer> {

    @Override
    public void write(final JsonWriter writer, final OfflinePlayer value) throws IOException {
        if (value == null) {
            writer.nullValue();
            return;
        }
        final String uuid = value.getUniqueId().toString();
        writer.value(uuid);
    }

    @Override
    public OfflinePlayer read(final JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }
        return Bukkit.getOfflinePlayer(UUID.fromString(reader.nextString()));
    }
}