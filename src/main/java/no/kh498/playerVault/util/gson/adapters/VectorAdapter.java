package no.kh498.playerVault.util.gson.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import org.bukkit.util.Vector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * @author kh498
 * @since 0.1.0
 */
public class VectorAdapter extends TypeAdapter<Vector> {

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    private static final String X = "X";
    private static final String Y = "Y";
    private static final String Z = "Z";

    @Override
    public void write(final JsonWriter writer, final Vector vector) throws IOException {
        if (vector == null) {
            writer.nullValue();
            return;
        }
        writer.beginObject();
        writer.name(X).value(vector.getX());
        writer.name(Y).value(vector.getY());
        writer.name(Z).value(vector.getZ());

        writer.endObject();
    }

    @Override
    public Vector read(final JsonReader reader) throws IOException {
        if (reader.peek() == JsonToken.NULL) {
            reader.nextNull();
            return null;
        }

        double x = 0D;
        double y = 0D;
        double z = 0D;
        reader.beginObject();
        while (reader.hasNext()) {
            final String name = reader.nextName();
            switch (name) {
                case X:
                    x = reader.nextDouble();
                    break;
                case Y:
                    y = reader.nextDouble();
                    break;
                case Z:
                    z = reader.nextDouble();
                    break;
                default:
                    this.log.warn("Failed to find the correct value for " + name);

            }
        }
        reader.endObject();
        return new Vector(x, y, z);
    }


}
